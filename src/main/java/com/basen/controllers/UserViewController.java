package com.basen.controllers;

import com.basen.dtos.UserDTO;
import com.basen.entities.User;
import com.basen.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(path = "/user/view")
public class UserViewController {

    private UserService userService;

    @Autowired
    public UserViewController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = "/entity/{userId}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> viewEntity(@PathVariable("userId") String userId) {
        UserDTO userDTO = userService.viewEntity(userId);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/sub-entities/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Set> viewSubEntities(@PathVariable("userId") String userId) {
        Set subEntities = userService.viewSubEntities(userId);
        return new ResponseEntity<>(subEntities, HttpStatus.OK);
    }

    @RequestMapping(path = "/data/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Map> viewData(@PathVariable("userId") String userId) {
        Map data = userService.viewData(userId);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(path = "/find-all", method = RequestMethod.GET)
    public ResponseEntity<List> findAll() {
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }
}
