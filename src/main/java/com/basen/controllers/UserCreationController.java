package com.basen.controllers;

import com.basen.entities.User;
import com.basen.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user/creation")
public class UserCreationController {

    private UserService userService;

    @Autowired
    public UserCreationController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = "/{userType}/{userId}", method = RequestMethod.PUT)
    public ResponseEntity<User> addUser(@PathVariable("userId") String userId, @PathVariable("userType") String userType) {
        User user = userService.addUser(userId, userType);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}/delete", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUser(@PathVariable("userId") String userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>("The following user id: " + userId + " has been successfully deleted.", HttpStatus.OK);
    }
}
