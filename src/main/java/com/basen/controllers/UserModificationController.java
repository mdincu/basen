package com.basen.controllers;

import com.basen.dtos.UserDTO;
import com.basen.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(path = "/user/modification")
public class UserModificationController {

    private UserService userService;

    @Autowired
    public UserModificationController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = "/add-data/{userId}/{key}/{value}", method = RequestMethod.POST)
    public ResponseEntity<UserDTO> addData(@PathVariable("userId") String userId, @PathVariable("key") String key, @PathVariable("value") String value) {
        userService.addData(userId, key, value);
        return new ResponseEntity<>(userService.viewEntity(userId), HttpStatus.OK);
    }

    @RequestMapping(path = "/add-sub-entity/{toUserId}/{userId}", method = RequestMethod.POST)
    public ResponseEntity<UserDTO> addSubEntity(@PathVariable("toUserId") String toUserId, @PathVariable("userId") String userId) {
        userService.addSubEntity(toUserId, userId);
        return new ResponseEntity<>(userService.viewEntity(toUserId), HttpStatus.OK);
    }
}
