package com.basen.dtos;

import java.util.*;

public class UserDTO {

    private String id;
    private Set<String> subEntities;
    private Map<String, String> data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<String> getSubEntities() {
        return subEntities;
    }

    public void setSubEntities(Set<String> subEntities) {
        this.subEntities = subEntities;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
