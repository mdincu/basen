package com.basen.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class InitUsersService {

    private UserService userService;

    @Autowired
    public InitUsersService(UserService userService) {
        this.userService = userService;
    }

    public void init() {
        addUsers();
        addData();
        addSubEntities();
    }

    private void addUsers(){
        List<String> users = Arrays.asList("Michael", "Georgia", "John", "Angela", "Alex", "Rita", "Jackson");
        users.stream().forEach(user -> userService.addUser(user, "regular"));
    }

    private void addData(){
        userService.addData("Michael", "School", "Chicago school");
        userService.addData("Michael", "Role", "Student");
        userService.addData("Michael", "Year", "2");

        userService.addData("Georgia", "School", "Chicago school");
        userService.addData("Georgia", "Role", "Student");
        userService.addData("Georgia", "Year", "1");

        userService.addData("John", "School", "Chicago school");
        userService.addData("John", "Role", "Teacher");
        userService.addData("John", "Subject", "Mathematics");

        userService.addData("Angela", "School", "Chicago school");
        userService.addData("Angela", "Role", "Teacher");
        userService.addData("Angela", "Subject", "Economics");

        userService.addData("Alex", "School", "Chicago school");
        userService.addData("Alex", "Role", "Guard");
        userService.addData("Alex", "Working time", "9:00 AM to 5:00 PM, Monday to Friday");

        userService.addData("Rita", "School", "Chicago school");
        userService.addData("Rita", "Role", "Cleaner");
        userService.addData("Rita", "Working time", "9:00 AM to 5:00 PM, Monday to Friday");
        userService.addData("Rita", "Floor", "Ground floor");

        userService.addData("Jackson", "School", "Chicago school");
        userService.addData("Jackson", "Role", "Head Teacher");
        userService.addData("Jackson", "Subject", "History");
    }

    private void addSubEntities(){
        userService.addSubEntity("Michael","John");
        userService.addSubEntity("Michael","Angela");
        userService.addSubEntity("Georgia","Angela");
        userService.addSubEntity("Georgia","Jackson");
        userService.addSubEntity("John","Michael");
        userService.addSubEntity("Jackson","Rita");
        userService.addSubEntity("Jackson","Alex");
        userService.addSubEntity("Jackson","Angela");
        userService.addSubEntity("Jackson","John");
        userService.addSubEntity("Jackson","Georgia");
    }
}
