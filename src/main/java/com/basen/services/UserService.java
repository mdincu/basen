package com.basen.services;

import com.basen.dtos.UserDTO;
import com.basen.entities.RegularUser;
import com.basen.entities.User;
import com.basen.exceptions.UserNotFoundException;
import com.basen.exceptions.UserTypeException;
import com.basen.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User addUser(String id, String userType) {
        User user;
        switch (userType) {
            case "regular":
                user = new RegularUser(id);
                break;
            default:
                throw new UserTypeException("The following user type: " + userType + " does not exist.");
        }
        return this.userRepository.save(user);
    }

    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }

    public void addData(String userId, String key, String value) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.getData().put(key, value);
        } else {
            throw new UserNotFoundException("The following user id: " + userId + " has not been found.");
        }
    }

    public void addSubEntity(String toUserId, String userId) {
        Optional<User> toUserOptional = userRepository.findById(toUserId);
        Optional<User> userOptional = userRepository.findById(userId);
        if (toUserOptional.isPresent() && userOptional.isPresent()) {
            User toUser = toUserOptional.get();
            User user = userOptional.get();
            toUser.getSubEntities().add(user);
        } else {
            throw new UserNotFoundException("The following user id: " + userId + " has not been found.");
        }
    }

    public UserDTO viewEntity(String userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            return mapUserToUserDTO(userOptional.get());
        } else {
            throw new UserNotFoundException("The following user id: " + userId + " has not been found.");
        }
    }

    public Set viewSubEntities(String userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            return getSubEntities(userOptional.get());
        } else {
            throw new UserNotFoundException("The following user id: " + userId + " has not been found.");
        }
    }

    public Map viewData(String userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            return userOptional.get().getData();
        } else {
            throw new UserNotFoundException("The following user id: " + userId + " has not been found.");
        }
    }

    public List<UserDTO> findAll() {
        List<User> users = userRepository.findAll();
        return users
                .stream()
                .map(user -> mapUserToUserDTO(user))
                .collect(Collectors.toList());
    }

    private UserDTO mapUserToUserDTO(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getID());
        userDTO.setData(user.getData());
        Set<String> entities = ((Set<User>) user.getSubEntities())
                .stream()
                .map(u -> u.getID())
                .collect(Collectors.toSet());
        userDTO.setSubEntities(entities);
        return userDTO;
    }

    private Set<String> getSubEntities(User user){
        return ((Set<User>) user.getSubEntities())
                .stream()
                .map(u -> u.getID())
                .collect(Collectors.toSet());
    }


}
