package com.basen.repositories;

import com.basen.entities.User;
import com.basen.exceptions.UserConflictException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserRepository{

    private List<User> users;

    public UserRepository() {
        this.users = new ArrayList<>();
    }

    public User save(User user) {
        Optional<User> userOptional = findById(user.getID());
        if(userOptional.isPresent()){
            throw new UserConflictException("The following user: "+ user.getID() + " already exists.");
        } else {
            this.users.add(user);
            return user;
        }
    }

    public void deleteById(String id) {
        for(int i=0; i<users.size(); i++){
            User user = users.get(i);
            if(user.getID().equals(id)){
                users.remove(user);
                return;
            }
        }
    }

    public Optional<User> findById(String userId) {
        for(User user: users){
            if(user.getID().equals(userId)){
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    public List<User> findAll() {
        return users;
    }
}
