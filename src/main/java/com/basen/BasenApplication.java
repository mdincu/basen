package com.basen;

import com.basen.services.InitUsersService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BasenApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(BasenApplication.class, args);
		run.getBean(InitUsersService.class).init();
	}
}
