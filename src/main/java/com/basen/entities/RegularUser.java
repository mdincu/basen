package com.basen.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RegularUser implements User {

    private String id;

    private Set<User> subEntities;

    private Map<String, String> data;

    {
        this.subEntities = new HashSet<>();
        this.data = new HashMap<>();
    }

    public RegularUser(String id) {
        this.id = id;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public Set getSubEntities() {
        return this.subEntities;
    }

    @Override
    public Map getData() {
        return this.data;
    }
}
