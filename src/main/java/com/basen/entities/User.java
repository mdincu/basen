package com.basen.entities;

import java.util.Map;
import java.util.Set;

public interface User {
    String getID();

    Set getSubEntities();

    Map getData();
}
