package com.basen.exceptions;

public class UserConflictException extends RuntimeException{

    public UserConflictException(String message){
        super(message);
    }
}
