package com.basen.exceptions;

public class UserTypeException extends RuntimeException {

    public UserTypeException(String message) {
        super(message);
    }
}
