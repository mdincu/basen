$(document).ready(function(){

    function init(){
        $("#viewEntityId").val('Michael');
        $("#displayEntityId").text('Michael');

        $("#viewEntityIdSubEntities").val('Michael');
        $("#displayEntityIdSubEntities").text('Michael');

        $("#viewEntityData").val('John');
        $("#displayEntityDataSpan").text('John');

        $("#entityId").val('Leo');
        $("#insertEntity").text('Leo');

        $("#entityOneId").val('Alex');
        $("#entityOne").text('Alex');
        $("#entityTwoId").val('Rita');
        $("#entityTwo").text('Rita');


        $("#dataEntityId").val('Jackson');
        $("#addDataEntity").text('Jackson');
        $("#dataKey").val('Years Active');
        $("#dataKeySpan").text('Years Active');
        $("#dataValue").val('8');
        $("#dataValueSpan").text('8');
    }

    init();

    function displayAllEntities(){
      $.ajax({
          type: "get",
          url: "http://localhost:8085/user/view/find-all/",
          success:  function( data ) { $("#displayAllEntities").text(JSON.stringify(data)); },
      });
    }

    displayAllEntities();

  $("#viewEntityButton").click(
      function(){
        var value = $("#viewEntityId").val();
        $.ajax({
            type: "get",
            url: "http://localhost:8085/user/view/entity/" + value,
            success:    function( data ) { $("#displayEntity").text(JSON.stringify(data)); },
            error : function( error ){ $("#displayEntity").text("There is no entity with the following id: " + value +"."); }
        });
      }
  );

  $("#viewEntityId").keyup(function(){
    $("#displayEntityId").text($("#viewEntityId").val());
  });

  $("#viewSubEntitiesButton").click(
      function(){
        var value = $("#viewEntityIdSubEntities").val();
        $.ajax({
            type: "get",
            url: "http://localhost:8085/user/view/sub-entities/" + value,
            success: function( data ) { $("#displaySubEntities").text(JSON.stringify(data)); },
            error : function( error ){ $("#displaySubEntities").text("There is no entity with the following id: " + value +"."); }
        });
      }
  );

  $("#viewEntityIdSubEntities").keyup(function(){
    $("#displayEntityIdSubEntities").text($("#viewEntityIdSubEntities").val());
  });

  $("#viewEntityDataButton").click(
      function(){
        var value = $("#viewEntityData").val();
        $.ajax({
            type: "get",
            url: "http://localhost:8085/user/view/data/" + value,
            success: function( data ) { $("#displayEntityData").text(JSON.stringify(data)); },
            error : function( error ){ $("#displayEntityData").text("There is no entity with the following id: " + value +"."); }
        });
      }
  );

  $("#viewEntityData").keyup(function(){
    $("#displayEntityDataSpan").text($("#viewEntityData").val());
  });

  $("#entityId").keyup(function(){
      $("#insertEntity").text($("#entityId").val());
    });

  $("#insertEntityButton").click(
      function(){
        var value = $("#entityId").val();
        $.ajax({
            type: "put",
            url: "http://localhost:8085/user/creation/regular/" + value,
            success: function( data ) {
                $("#displayInsert").text(JSON.stringify(data));
                displayAllEntities();
            },
            error : function( error ){ $("#displayInsert").text("There is already an user defined with the following id: " + value +"."); }
        });
      }
    );

    $("#entityOneId").keyup(function(){
      $("#entityOne").text($("#entityOneId").val());
    });

    $("#entityTwoId").keyup(function(){
      $("#entityTwo").text($("#entityTwoId").val());
    });

    $("#mapEnititiesButton").click(
          function(){
            var entityOne = $("#entityOneId").val();
            var entityTwo = $("#entityTwoId").val();
            $.ajax({
                type: "post",
                url: "http://localhost:8085/user/modification/add-sub-entity/" + entityOne + "/" + entityTwo,
                success: function( data ) {
                    $("#displayMap").text(JSON.stringify(data));
                },
            });
          }
        );

    $("#dataEntityId").keyup(function(){
      $("#addDataEntity").text($("#dataEntityId").val());
    });

    $("#dataKey").keyup(function(){
      $("#dataKeySpan").text($("#dataKey").val());
    });

    $("#dataValue").keyup(function(){
      $("#dataValueSpan").text($("#dataValue").val());
    });

    $("#addDataButton").click(
          function(){
            var dataEntityId = $("#dataEntityId").val();
            var dataKey = $("#dataKey").val();
            var dataValue = $("#dataValue").val();
            $.ajax({
                type: "post",
                url: "http://localhost:8085/user/modification/add-data/" + dataEntityId + "/" + dataKey + "/" + dataValue,
                success: function( data ) {
                    $("#displayNewData").text(JSON.stringify(data));
                },
            });
          }
        );
});